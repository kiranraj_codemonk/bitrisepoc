/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../App';
const sum = require('../sum');

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  renderer.create(<App />);
});

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(4);
});
